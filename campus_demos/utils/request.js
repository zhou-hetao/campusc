
import axios from 'axios'

export function request(config) {
    const axios1 = axios.create({
        baseURL: "http://127.0.0.1:7001",
        timeout: 5000
    })
    return axios1(config)
}