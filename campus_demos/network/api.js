
import { request } from "../utils/request.js"
export const axiosapi =  {
 getUserList(){
  return request({
      url:'/api/userlist',
      method:'GET',
  })
 },
 zqh_namelist(){
     return request({
         url:'/api/namelist',
         method:'GET',
     })
 },
 zqh_namelistid(data){
     return request({
         url:'/api/namelist/'+data,
         method:'GET',
     })
 },
 zqh_logo(){
     return request({
         url:'/api/logo',
         method:'GET',
     })
 },
 zqh_fabiaolist(){
     return request({
         url:'/api/fabiaolist',
         method:'GET',
     })
 },
 zqh_fenleilist(){
     return request({
         url:'/api/fenleilist',
         method:'GET',
     })
 },
 zqh_shijianlist(){
     return request({
         url:'/api/shijianlist',
         method:'GET',
     })
 },
 zqh_namelistpinglun(data){
	 console.log(data);
     return request({
         url:'/api/namelistpinglun/'+data.id,
         method:'put',
		 data:data
     })
 },
 zqh_fabiao(data){
 	 console.log(data);
     return request({
         url:'/api/fabiao',
         method:'post',
 		 data:data
     })
 },
 zqh_fenlei(data){
 	 console.log(data);
     return request({
         url:'/api/fenlei',
         method:'post',
 		 data:data
     })
 },
 zqh_shijian(data){
 	 console.log(data);
     return request({
         url:'/api/shijian',
         method:'post',
 		 data:data
     })
 },
 zqh_addlist(data){
 	 console.log(data);
     return request({
         url:'/api/addlist',
         method:'post',
 		 data:data
     })
 },
 lg_getlist(obj){
     return request({
         url:'/api/getLGcsc',
         method:'GET',
     })
 },
 lg_addList(data) {
 	 console.log(data);
  return request({
   url: '/api/addLGList',
   method: 'POST',
   data: data,
  })
 },
 getlgpl: function(data) {
   return request({
    url: "/api/getlgpl", //请求头
    method: "GET", //请求方式
    data: data, //请求数据
   })
  },
   //彩生活获取
   getxjycolorful(obj){
       return request({
           url:'/api/getxjycolorful',
           method:'GET',
       })
   },
   //获取用户信息
   getxjyaccount(data) {
   	return request({
   		url: "/api/getxjyaccount", //请求头
   		method: "GET", //请求方式
   		data: data, //请求数据
   	})
   },
   //添加新用户
   addxjyaccount: function(data) {
  		return request({
  			url: '/api/addxjyaccount',
  			method: 'POST',
  			data: data,
  		})
  	},
  	// 查找用户信息
  	getseekxjyaccount: function(data) {
  		return request({
  			url: `/api/getseekxjyaccount/${data.id}`,
  			method: 'GET',
  			data: data,
  		})
  	},
  	//更改用户信息
  	upxjyaccount: function(data) {
  		return request({
  			url: `/api/upxjyaccount/${data.id}`,
  			method: 'PUT',
  			data: data,
  		})
  	},
  	//获取token
  	xjylogin:function(data) {
  		return request({
  			url: "/api/xjylogin", //请求头
  			method: "POST", //请求方式
  			data: data, //请求数据
  		})
  	},
	//lg添加评论
	lgAddPl:function(data){
		return request(({
			url:"/api/addlgpl",
			method:"POST",
			data:data
		}))
	},
	xjygetshop:function(data){
		return request({
			url:"/api/getxjyaccount/"+data.id,
			method:"GET"
		})
	},
	lgputshop:function(data){
		return request({
			url:"/api/putLGcsc/"+data.id,
			method:"PUT",
			data:data
		})
	},
	zqhputhuodong:function(data){
		return request({
			url:"/api/putnamelist/"+data.id,
			method:"PUT",
			data:data
		})
	}
}

 
 /*
 // 请求模板
 请求样式：
     自定义名字: function(data) {
         return request({
             url: "/banner", //请求头
             method: "GET", //请求方式 
             data: data,    //请求数据
             token: token, // 可传  
             hideLoading: false, //加载样式
         })
     },
 */
 /*
 
 // 获取
  getMsg: function(data) {
   return request({
    url: "/api/yytadress", //请求头
    method: "GET", //请求方式
    data: data, //请求数据
   })
  },
  // 删除
  delMsg: function(data) {
   return request({
    url: `/api/yytdelbanner/${data.id}`,
    method: 'DELETE',
    data: data,
   })
  },
  // 添加
  addMsg: function(data) {
   return request({
    url: '/api/yytaddbanner',
    method: 'POST',
    data: data,
   })
  },
  // 更新
  upMsg: function(data) {
   return request({
    url: `/api/yytupbanner/${data.id}`,
    method: 'PUT',
    data: data,
   })
  },
  // 查找
  searchMsg: function(data) {
   return request({
    url: `/api/yytbanner/${data.id}`,
    method: 'GET',
    data: data,
   })
  }*/
// export function getUserList(obj){
//     return request({
//         url:'/api/userlist',
//         method:'GET',
//     })
// }
