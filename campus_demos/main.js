import App from './App'
import uView from "uview-ui";
import {axiosapi} from "./network/api.js"
Vue.prototype.$api = axiosapi
Vue.use(uView);
import './static/icon/iconfont.css'
// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false


// 全局过滤器——价钱补0
Vue.filter("formatPrice",(val) => {
	return `${parseFloat(val).toFixed(2)}`
})

App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif