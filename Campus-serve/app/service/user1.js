'use strict';

const Service = require('egg').Service;

class user1 extends Service {
  async findAll() {
    const result = await this.app.mysql.select('user1');
    return {
      code: 200,
      msg: '查找成功',
      data: result,
    };
  }
  async find(uid) {
    const result = await this.app.mysql.get('user1', { id: uid });
    if (result !== null) {
      return {
        code: 200,
        msg: '查找成功',
        data: result,
      };
    }
    return {
      msg: '没有此条数据！',
    };

  }
  async create(params) {
    const cfg = {
      id: params.id,
      name: params.name,
      age: params.age,
      img: params.img,
    };
    const result = await this.app.mysql.insert('user1', cfg);
    return {
      code: 200,
      msg: '创建成功',
      data: result,
    };
  }
  async delete(uid) {
    const result = await this.app.mysql.delete('user1', { id: uid });
    return {
      code: 200,
      msg: '删除成功',
      data: result,
    };
  }
  async update(uid, params) {
    const cfg = {
      id: uid,
      name: params.name,
      age: params.age,
      img: params.img,
    };
    const result = await this.app.mysql.update('user1', cfg);
    return {
      code: 200,
      msg: '更新成功',
      data: result,
    };
  }
  async findAll0(pass) {
    const result = await this.app.mysql.select('user1', {
      where: { pass },
    });
    return {
      code: 200,
      msg: '查找成功',
      data: result,
    };
  }
}

module.exports = user1;
