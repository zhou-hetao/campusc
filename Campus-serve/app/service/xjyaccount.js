'use strict'

const Service = require('egg').Service

class xjyaccount extends Service {
  async findAll() {
    const result = await this.app.mysql.select('xjyaccount')
    return {
      code: 200,
      msg: '查找成功',
      data: result,
    }
  }
  async find(uid) {
    const result = await this.app.mysql.get('xjyaccount', { id: uid })
    if (result !== null) {
      return {
        code: 200,
        msg: '查找成功',
        data: result,
      }
    } else {
      return {
        msg: '没有此条数据！',
      }
    }
  }
  async create(params) {
    const cfg = {
      ...params
    }
    const result = await this.app.mysql.insert('xjyaccount', cfg)
    return {
      code: 200,
      msg: '创建成功',
      data: result,
    }
  }
  async delete(uid) {
    const result = await this.app.mysql.delete('xjyaccount', { id: uid })
    return {
      code: 200,
      msg: '删除成功',
      data: result,
    }
  }
  async update(uid, params) {
    const cfg = {
      ...params
    }
    const result = await this.app.mysql.update('xjyaccount', cfg)
    return {
      code: 200,
      msg: '更新成功',
      data: result,
    }
  }
  async findAll0(pass) {
    const result = await this.app.mysql.select('xjyaccount', {
      where: { pass: pass },
    })
    return {
      code: 200,
      msg: '查找成功',
      data: result,
    }
  }
}

module.exports = xjyaccount
