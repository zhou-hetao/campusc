'use strict';

const Service = require('egg').Service;

class jwt extends Service {
  async getUserByName(account) {
    const { app } = this;
    try {
      const result = await app.mysql.get('xjyaccount', { account });
      return result;
    } catch (error) {
      console.log(error);
      return null;
    }
  }
}

module.exports = jwt;