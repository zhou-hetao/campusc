'use strict'

const Service = require('egg').Service

class lgpl extends Service {
  async findAll() {
    const result = await this.app.mysql.select('lgpl')
    return {
      code: 200,
      msg: '查找成功',
      data: result,
    }
  }
  async find(uid) {
    const result = await this.app.mysql.get('lgpl', { id: uid })
    if (result !== null) {
      return {
        code: 200,
        msg: '查找成功',
        data: result,
      }
    } else {
      return {
        msg: '没有此条数据！',
      }
    }
  }
  async create(params) {
    const cfg = {
      ...params
    }
    const result = await this.app.mysql.insert('lgpl', cfg)
    return {
      code: 200,
      msg: '创建成功',
      data: result,
    }
  }
  async delete(uid) {
    const result = await this.app.mysql.delete('lgpl', { id: uid })
    return {
      code: 200,
      msg: '删除成功',
      data: result,
    }
  }
  async update(uid, params) {
    const cfg = {
      id: uid,
      name: params.name,
      age: params.age,
      img: params.img,
    }
    const result = await this.app.mysql.update('lgpl', cfg)
    return {
      code: 200,
      msg: '更新成功',
      data: result,
    }
  }
  async findAll0(pass) {
    const result = await this.app.mysql.select('lgpl', {
      where: { pass: pass },
    })
    return {
      code: 200,
      msg: '查找成功',
      data: result,
    }
  }
}

module.exports = lgpl
