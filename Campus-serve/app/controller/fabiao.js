'use strict';

const Controller = require('egg').Controller;

class fabiao extends Controller {
  async index() {
    const data = await this.ctx.service.fabiao.findAll(this.ctx);
    this.ctx.body = data;
  }
  async create() {
    const ctx = this.ctx;
    const params = ctx.request.body;
    const data = await ctx.service.fabiao.create(params);
    ctx.body = data;
  }
  async destroy() {
    const ctx = this.ctx;
    const userId = ctx.params.id;
    const del = await ctx.service.fabiao.delete(userId);
    ctx.body = del;
  }
  async update() {
    const ctx = this.ctx;
    const userId = ctx.params.id;
    const params = ctx.request.body;
    const change = await ctx.service.fabiao.update(userId, params);
    ctx.body = change;
  }
  async show() {
    const ctx = this.ctx;
    const id = ctx.params.id;
    const user = await this.ctx.service.fabiao.find(id);
    ctx.body = user;
  }
  async index0() {
    const ctx = this.ctx;
    const pass = ctx.params.age;
    console.log(pass);
    const data = await this.ctx.service.fabiao.findAll0(pass);
    this.ctx.body = data;
  }
}
module.exports = fabiao;
