'use strict';

/**+
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  router.get('/', controller.home.index);
   // router.get('/api/test', controller.test.index)
  //token
  router.post('/api/xjylogin', controller.loginjwt.login);
  //彩生活获取
  router.get('/api/getxjycolorful', controller.xjycolorful.index);
  router.get('/api/getxjyaccount/:id', controller.xjycolorful.show)
  //注册
  router.get('/api/getxjyaccount', controller.xjyaccount.index)
  router.get('/api/getseekxjyaccount/:id', controller.xjyaccount.show) //查找
  router.post('/api/addxjyaccount', controller.xjyaccount.create) //创建
  router.put('/api/upxjyaccount/:id', controller.xjyaccount.update)//更改用户信息
  router.get('/api/namelist', controller.namelist.index);
  router.get('/api/logo', controller.logo.index);
  router.get('/api/namelist/:id', controller.namelist.show); // 详情页
  router.put('/api/putnamelist/:id', controller.namelist.update);
  router.put('/api/namelistpinglun/:id', controller.namelist.update);// 发布评论
  router.post('/api/fabiao', controller.fabiao.create);// 发表组局
  router.post('/api/fenlei', controller.fenlei.create);// 发表分类
  router.post('/api/shijian', controller.shijian.create);// 发表组局
  router.get('/api/fabiaolist', controller.fabiao.index);// 查找组局
  router.get('/api/fenleilist', controller.fenlei.index);// 查找分类
  router.get('/api/shijianlist', controller.shijian.index);// 查找分类
  router.post('/api/addlist', controller.namelist.create);
  //彩市场获取数据
  router.get('/api/getLGcsc', controller.lgcsc.index);
  router.put('/api/putLGcsc/:id', controller.lgcsc.update);
  // 彩市场发布
  router.post('/api/addLGList', controller.lgcsc.create);
  // 上传图片
  router.post('/api/lgupload', controller.lGUpload.updatepuSubPic);
  //评论获取
  router.get('/api/getlgpl', controller.lgpl.index)
  router.post('/api/addlgpl', controller.lgpl.create)
  router.get('/api/userlist', controller.userlist.index);
  // 上传图片
  router.post("/api/zhtupdate",controller.zhtUpload.updatepuSubPic);
}
