// ==UserScript==
// @name         新国开答题脚本
// @namespace    http://tampermonkey.net/
// @version      0.23
// @description  学习新思想，争做新青年！
// @author       DMC52859
// @match        https://lms.ouchn.cn/exam/*
// @match        https://lms.ouchn.cn/course/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=ouchn.cn
// @require      https://greasyfork.org/scripts/454504-%E5%9B%BD%E5%BC%80%E9%A2%98%E5%BA%93/code/%E5%9B%BD%E5%BC%80%E9%A2%98%E5%BA%93.js?version=1128321
// @grant        none
// @license      MIT
// ==/UserScript==

(function() {
    'use strict';
    var jsonData;
     setTimeout(function(){
        jsonData = func()
        console.log(jsonData)
        console.log('调用成功')
     },3000)
     window.addEventListener('load',function(){
        var test_url = window.location.href
        console.log(test_url)
        if(test_url.includes('learning-activity#/exam')){
            console.log('这是考试结束界面')
            window.open(localStorage.getItem("URL"),'_selft')
        }
        var class_url = window.location.href
        if(class_url.includes('full-screen')){
            console.log('这是刷课界面')
            localStorage.setItem("URL",class_url)
        }
        let fun = function(){
            var file = document.getElementsByClassName('file-name ng-binding')
            var test = document.getElementsByClassName('button button-green take-exam ng-scope')
            var next = document.getElementsByClassName('next ng-binding ng-scope')[0]
            var content = document.getElementsByClassName('activity-content-bd material-box')
            //var audio = document.querySelectorAll("#root > div > section > div > div > div > div > div:nth-child(2) > section > div > div > div > div > div > div > div.render-detail-article-content > div.render-detail-content.cke-mode > div > audio")
            if(file.length>0 && content.length>0){
                //console.log('这是文件')
                document.getElementsByClassName('font font-table-edit-view')[0].click()
                try{
                    var video1 = document.getElementsByTagName('video')[0]
                    video1.play();
                    video1.muted = true;
                    setTimeout("document.getElementsByClassName('font font-close')[0].click()", 5000)
                    next.click();
                }catch(error){
                    setTimeout("document.getElementsByClassName('font font-close')[0].click()", 5000)
                    next.click();
                }
            }else if($('video').length>0){
                //console.log('这是视频')
                var video = document.getElementsByTagName('video')[0];
                var time = video.duration // 视频总时长
                var currenttime = video.currentTime // 当前时长
                if(document.querySelector('video').paused){
                        document.getElementsByClassName('mvp-fonts mvp-fonts-play')[0].click();
                        video.playbackRate = 10 ; //控制视频播放速度：2倍速
                        video.muted = true;
                        if(currenttime == time){
                            document.getElementsByClassName('next ng-binding ng-scope')[0].click();
                        }
                    }else if(document.querySelector('video').played){
                        if(currenttime == time){
                        document.getElementsByClassName('next ng-binding ng-scope')[0].click();
                        }
                    }
            }else if(document.querySelectorAll("body > div.wrapper > div.main-content.gtm-category > div.content-under-nav-2.with-loading.exam-activity-container.ng-scope > div.bd > div > div > div.exam-area-content > div > div.paper-content.card > div").length>0){
                //console.log('开始考试')
                $('.exam-subjects ol li').each(function(){
                        var self1 = $(this)
                        var classname = self1.attr('class')
                        //console.log(classname)
                        if(classname == 'subject ng-scope single_selection'){
                            console.log('这是单选题')
                            // 获取题目
                            var q1 = self1.children('div').children('div').children('div').children('span').children('p')
                            var que1 = q1.text().replace(/[–!.?&\|\\\*^%$#@\-_—。，“”"" 【】→（  ）、()­？：\s+]/g,"")
                            console.log(que1)
                            // 获取JSON答案
                            var da_an1 = jsonData[que1]
                            console.log('答案：'+da_an1)
                            // 获取选项
                            var xuanxiang1;
                            if(self1.children('div').children('div').eq(1).children('ol').children('li').children('label').children('div').children('span').children('p').length==0){
                                xuanxiang1 = self1.children('div').children('div').eq(1).children('ol').children('li').children('label').children('div').children('span')
                            }else{
                                xuanxiang1 = self1.children('div').children('div').eq(1).children('ol').children('li').children('label').children('div').children('span').children('p')
                            }
                            //var xx = self1.children('div').children('div').eq(1).children('ol').children('li')
                            var ans1 = [];
                            for(var i=0;i<xuanxiang1.length;i++){
                                ans1[i] = xuanxiang1[i].innerText.replace(/[，、,''""‘’“”.。|\n\s+]/g,"")
                                console.log(ans1[i])
                            }
                            for(var a=0;a<ans1.length;a++){
                                // console.log(ans1[a])
                                if(ans1[a] == da_an1){
                                    console.log('匹配成功')
                                    var xx1 = self1.children('div').children('div').eq(1).children('ol').children('li').children('label')[a]
                                    xx1.click();
                                    console.log(xx1)
                                }
                            }
                        }else{
                next.click();
                      }
        }
        setInterval(function(){
            fun();
        }, 8000)
    })
 //setInterval(skip,200)
})();
function skip() {
     let video = document.getElementsByTagName('video')
     for (let i=0; i<video.length; i++) {
         video[i].currentTime = video[i].duration
     }
 }
